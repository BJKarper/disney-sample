# Disney Sample

Disney Sample app is a simple app that consumes the marvel developer api to present a detail view of a comic book.<br/>
Displaying a comic book's cover image, title, and description or series description if the individual issue doesn't have a description.

<img align="right" width="222" height="480" src="https://media.giphy.com/media/1mdUwVMHSQuujq8qNP/giphy.gif">

Architecture
--

This application is designed using MVVM architecture and is organzied in a multimodule file structure as follows:

- app
- buildSrc -- Dependency Management
- core
  - network-core -- Provides Dagger Modules for Retrofit
- features
  - comics
    - comics-detail -- UI module for Comic Detail View
    - comics-data -- module for datalayer of the comics api and repository
    - comics-detail-data -- module for datalayer of the comics and series api's and repositories
  - series
    - series-data -- module for datalayer of the series api and repository


As the app grows in features they will be given their own new folder section under features<br/>
i.e. (features >> feature1 >> feature1/feature1-data)

When the app grows to a point where reusable UI components can be created or a unified app design is settled on.<br/>
A new section under core can be created. (core >> compose-core)

ApiKey Setup
---
Setting up the api to use your own apikeys from Marvel developer settings is easy.<br/>
Create a file to the root apikey.properties<br/> 
Then add the following fields replacing XXX's with your personal keys.

```
PUBLIC_KEY="XXXXXXXXXXXX"
PRIVATE_KEY="XXXXXXXXXXXX"
```

Libraries
--
The app utilizes the following libraries/languages:

Development Libraries
- Kotlin
- Kotlin-Coroutines
- AndroidX
- Compose
- Compose Navigation
- Coil
- Dagger Hilt
- Retrofit
- OkHttp
- Jackson

Testing Libraries
- Mockk
- JUnit Test
  - Compose Test
  - Coroutines Test
- Hilt Test
