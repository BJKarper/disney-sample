package co.bjkarper.sample.nav

object NavDestinations {
    const val SEARCH = "Search"
    const val DETAILS = "Details"
}