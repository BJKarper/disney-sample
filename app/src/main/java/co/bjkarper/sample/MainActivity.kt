package co.bjkarper.sample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import co.bjkarper.features.comics_detail.ComicDetailViewModel
import co.bjkarper.features.comics_detail.ui.ComicDetailContent
import co.bjkarper.features.comics_detail.ui.ComicDetailErrorContent
import co.bjkarper.features.comics_detail.ui.ComicSearchContent
import co.bjkarper.sample.nav.NavDestinations
import co.bjkarper.sample.ui.SampleScaffold
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val comicDetailViewModel by viewModels<ComicDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MaterialTheme {
                val navController = rememberNavController()
                val showError by comicDetailViewModel.showError.collectAsState(initial = false)
                val isLoading by comicDetailViewModel.isLoading.collectAsState(initial = false)

                SampleScaffold(navController) {
                    NavHost(
                        navController = navController,
                        startDestination = NavDestinations.SEARCH
                    ) {
                        composable(NavDestinations.SEARCH) {
                            ComicSearchContent(
                                comicDetailViewModel,
                                navController
                            )
                        }
                        composable(
                            NavDestinations.DETAILS,
                        ) {
                            SwipeRefresh(
                                state = rememberSwipeRefreshState(isLoading),
                                onRefresh = { comicDetailViewModel.refresh() }) {
                                if (showError) {
                                    ComicDetailErrorContent()
                                } else {
                                    ComicDetailContent(comicDetailViewModel, navController)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}