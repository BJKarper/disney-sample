package co.bjkarper.sample.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import co.bjkarper.sample.nav.NavDestinations

@Composable
fun SampleScaffold(
    navController: NavController,
    content: @Composable (PaddingValues) -> Unit
) {
    Scaffold(
        topBar = { SampleAppBar(navController = navController) },
        content = content
    )
}

@Composable
fun SampleAppBar(navController: NavController) {
    val currentBackStackEntry by navController.currentBackStackEntryAsState()
    val label = currentBackStackEntry?.destination?.route ?: ""

    if (currentBackStackEntry?.destination?.route != NavDestinations.DETAILS) {
        TopAppBar(
            title = { Text(text = label) },
            backgroundColor = MaterialTheme.colors.primary,
        )
    } else {
        TopAppBar(
            title = { Text(text = label) },
            backgroundColor = MaterialTheme.colors.primary,
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(Icons.Default.ArrowBack, "Back Button")
                }
            }
        )
    }
}