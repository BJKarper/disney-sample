pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "Sample"
include(":app")

// Core Modules
include(":network-core")
project(":network-core").projectDir = File("core/network-core")

// Features
//// Comics Detail
include(":comics-detail")
project(":comics-detail").projectDir = File("features/comics/comics-detail")
include(":comics-detail-data")
project(":comics-detail-data").projectDir = File("features/comics/comics-detail-data")

include(":comics-data")
project(":comics-data").projectDir = File("features/comics/comics-data")

//// Series
include(":series-data")
project(":series-data").projectDir = File("features/series/series-data")
