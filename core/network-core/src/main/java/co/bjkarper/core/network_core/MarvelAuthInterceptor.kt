package co.bjkarper.core.network_core

import android.util.Log
import co.bjkarper.sample.core.network_core.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.security.MessageDigest


class MarvelAuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

//        val timeStamp = Date().time.toString()
        val timeStamp = "1659121431918"
        val md5Hash = getMD5HashString(timeStamp)
        val request = chain.request()

        val url = request.url.newBuilder()
            .addQueryParameter("apiKey", BuildConfig.PUBLIC_KEY)
            .addQueryParameter("ts", timeStamp)
            .addQueryParameter("hash", md5Hash)
            .build()

        Log.d("BJKARPER", "NEW URL: $url")

        val newRequest = request.newBuilder().url(url).build()
        return chain.proceed(newRequest)
    }

    private fun getMD5HashString(timeStamp: String): String {
        val hashBytes = MessageDigest.getInstance("MD5")
            .digest(("$timeStamp${BuildConfig.PRIVATE_KEY}${BuildConfig.PUBLIC_KEY}").toByteArray())

        return hashBytes.joinToString("") { "%02x".format(it) }
    }
}