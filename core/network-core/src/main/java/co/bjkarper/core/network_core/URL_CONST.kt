package co.bjkarper.core.network_core

const val BASE_COMICS_URL = "/v1/public/comics"
const val BASE_SERIES_URL = "/v1/public/series"