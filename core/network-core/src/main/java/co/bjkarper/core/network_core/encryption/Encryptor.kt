package co.bjkarper.core.network_core.encryption

import java.security.MessageDigest

interface IEncryptor {

    fun encrypt(timestamp: String, privateKey: String, publicKey: String): String
}

class MD5Encryptor(): IEncryptor {
    override fun encrypt(timestamp: String, privateKey: String, publicKey: String): String {
        val hashBytes = MessageDigest.getInstance("MD5")
            .digest(("$timestamp${privateKey}${publicKey}").toByteArray())

        return hashBytes.joinToString("") { "%02x".format(it) }
    }
}