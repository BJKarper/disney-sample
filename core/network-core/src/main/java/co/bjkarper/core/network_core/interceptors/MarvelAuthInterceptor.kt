package co.bjkarper.core.network_core.interceptors

import co.bjkarper.core.network_core.encryption.IEncryptor
import co.bjkarper.sample.core.network_core.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.util.*
import javax.inject.Inject

class MarvelAuthInterceptor @Inject constructor(
    private val encryptor: IEncryptor
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val timeStamp = Date().time.toString()
        val md5Hash = encryptor.encrypt(timeStamp, BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY)
        val request = chain.request()

        val url = request.url.newBuilder()
            .addQueryParameter("apikey", BuildConfig.PUBLIC_KEY)
            .addQueryParameter("ts", timeStamp)
            .addQueryParameter("hash", md5Hash)
            .build()

        val newRequest = request.newBuilder().url(url).build()
        return chain.proceed(newRequest)
    }
}