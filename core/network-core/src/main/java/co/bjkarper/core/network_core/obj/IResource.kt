package co.bjkarper.core.network_core.obj

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonDeserialize(`as` = Resource::class)
interface IResource {

    val resourceURI: String
    val name: String

    fun getResourceId(): Int
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Resource(
    @JsonProperty("resourceURI") override val resourceURI: String = "",
    @JsonProperty("name") override val name: String = "",
): IResource {
    override fun getResourceId(): Int = resourceURI.split("/").last().toInt()
}