package co.bjkarper.core.network_core.obj

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonDeserialize(`as` = Image::class)
interface IImage {

    val path: String
    val extension: String
}

data class Image(
    @JsonProperty("path") override val path: String = "",
    @JsonProperty("extension") override val extension: String = ""
) : IImage
