package co.bjkarper.features.series_data

import co.bjkarper.features.series_data.api.SeriesApi
import co.bjkarper.features.series_data.obj.Series
import co.bjkarper.features.series_data.obj.SeriesDataContainer
import co.bjkarper.features.series_data.obj.SeriesDataWrapper
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.HttpException
import retrofit2.Response


@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class SeriesRepositoryTests {

    private val mockApi = mockk<SeriesApi>()

    private val seriesRepository = SeriesRepository(mockApi)

    private val testSeries =
        Series(id = 1, title = "Test Series Title", description = "Test Series Description")
    private val testSeriesDataContainer = SeriesDataContainer(count = 1, results = listOf(testSeries))
    private val testSeriesDataWrapper =
        SeriesDataWrapper(code = "Ok", status = "200", data = testSeriesDataContainer)

    @Test
    fun `Test getSeriesById Returns Valid`() = runTest {
        coEvery { mockApi.getSeriesById(1) } returns testSeriesDataWrapper

        seriesRepository.getSeriesById(1).collect { result ->
            result.getOrNull()?.let {
                Assert.assertTrue(it.code == "Ok")
                Assert.assertTrue(it.data.count == 1)
                Assert.assertTrue(it.data.results.first().id == 1)
            }
        }
    }

    @Test
    fun `Test getSeriesById Returns Error`() = runTest {
        coEvery { mockApi.getSeriesById(1) } throws HttpException(
            Response.error<Any>(
                409,
                "Conflict".toResponseBody()
            )
        )

        seriesRepository.getSeriesById(1).catch {
            Assert.assertTrue(it::class == HttpException::class)
            Assert.assertTrue(it.message == "Conflict")
        }
    }
}