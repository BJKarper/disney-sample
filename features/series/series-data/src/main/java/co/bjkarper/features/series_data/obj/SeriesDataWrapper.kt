package co.bjkarper.features.series_data.obj

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class SeriesDataWrapper(

    @JsonProperty("code") val code: String = "",
    @JsonProperty("status")val status: String = "",
    @JsonProperty("copyright")val copyright: String = "",
    @JsonProperty("attributionText")val attributionText: String = "",
    @JsonProperty("attributionHTML")val attributionHTML: String = "",
    @JsonProperty("data")val data: SeriesDataContainer = SeriesDataContainer(),
    @JsonProperty("etag")val etag: String = ""
)