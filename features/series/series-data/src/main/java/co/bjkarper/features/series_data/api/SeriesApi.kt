package co.bjkarper.features.series_data.api

import co.bjkarper.core.network_core.BASE_SERIES_URL
import co.bjkarper.features.series_data.obj.SeriesDataWrapper
import retrofit2.http.GET
import retrofit2.http.Path


interface SeriesApi {

    @GET("$BASE_SERIES_URL/{seriesId}")
    suspend fun getSeriesById(@Path("seriesId") seriesId: Int): SeriesDataWrapper
}