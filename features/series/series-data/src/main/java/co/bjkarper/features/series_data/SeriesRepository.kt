package co.bjkarper.features.series_data

import co.bjkarper.features.series_data.api.SeriesApi
import co.bjkarper.features.series_data.obj.SeriesDataWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

interface ISeriesRepository {

    fun getSeriesById(seriesId: Int): Flow<Result<SeriesDataWrapper>>
}

class SeriesRepository @Inject constructor(
    private val seriesApi: SeriesApi
) : ISeriesRepository {

    override fun getSeriesById(seriesId: Int): Flow<Result<SeriesDataWrapper>> = flow {
        emit(Result.success(seriesApi.getSeriesById(seriesId)))
    }.catch { e ->
        emit(Result.failure(e))
    }.flowOn(Dispatchers.IO)

}