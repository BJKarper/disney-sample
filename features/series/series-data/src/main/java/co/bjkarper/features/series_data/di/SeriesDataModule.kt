package co.bjkarper.features.series_data.di


import co.bjkarper.features.series_data.ISeriesRepository
import co.bjkarper.features.series_data.SeriesRepository
import co.bjkarper.features.series_data.api.SeriesApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import retrofit2.Retrofit

@Module
@InstallIn(ViewModelComponent::class)
class SeriesDataModule {

    @Provides
    @ViewModelScoped
    fun providesComicRepository(api: SeriesApi): ISeriesRepository {
        return SeriesRepository(api)
    }

    @Provides
    @ViewModelScoped
    fun providesComicAPI(retrofit: Retrofit): SeriesApi {
        return retrofit.create(SeriesApi::class.java)
    }

}