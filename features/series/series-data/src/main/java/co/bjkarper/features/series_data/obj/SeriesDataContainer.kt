package co.bjkarper.features.series_data.obj

import com.fasterxml.jackson.annotation.JsonProperty

data class SeriesDataContainer(

    @JsonProperty("offset") val offset: Int = 0,
    @JsonProperty("limit") val limit: Int = 0,
    @JsonProperty("total") val total: Int = 0,
    @JsonProperty("count") val count: Int = 0,
    @JsonProperty("results") val results: List<Series> = emptyList()
)