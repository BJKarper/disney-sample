package co.bjkarper.features.series_data.obj

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Series(
    @JsonProperty val id: Int = 0,
    @JsonProperty val title: String = "",
    @JsonProperty val description: String = ""
)
