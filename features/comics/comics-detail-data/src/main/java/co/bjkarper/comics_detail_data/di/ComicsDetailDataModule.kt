package co.bjkarper.comics_detail_data.di

import co.bjkarper.comics_detail_data.repo.ComicDetailRepository
import co.bjkarper.comics_detail_data.repo.IComicDetailRepository
import co.bjkarper.features.comic_data.repo.IComicRepository
import co.bjkarper.features.series_data.ISeriesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class ComicsDetailDataModule {

    @Provides
    @ViewModelScoped
    fun providesComicDetailRepository(comicRepository: IComicRepository, seriesRepository: ISeriesRepository): IComicDetailRepository {
        return ComicDetailRepository(comicRepository, seriesRepository)
    }

}