package co.bjkarper.comics_detail_data.repo

import co.bjkarper.comics_detail_data.obj.ComicDetail
import co.bjkarper.comics_detail_data.obj.IComicDetail
import co.bjkarper.features.comic_data.repo.IComicRepository
import co.bjkarper.features.series_data.ISeriesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

interface IComicDetailRepository {

    fun getComicDetailById(comicId: Int): Flow<Result<IComicDetail>>
}

class ComicDetailRepository @Inject constructor(
    private val comicRepository: IComicRepository,
    private val seriesRepository: ISeriesRepository
) : IComicDetailRepository {

    override fun getComicDetailById(comicId: Int): Flow<Result<IComicDetail>> = flow {

        comicRepository.getComicById(comicId).collect { result ->
            when {
                result.isSuccess -> {
                    result.getOrNull()?.let { cw ->
                        cw.data.results.first().let { comic ->
                            seriesRepository.getSeriesById(comic.series.getResourceId())
                                .collect { result ->
                                    if (result.isSuccess) {
                                        result.getOrNull()?.let { sw ->
                                            sw.data.results.first().let { series ->
                                                emit(Result.success(ComicDetail(comic, series)))
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
                result.isFailure -> {
                    emit(Result.failure(result.exceptionOrNull()!!))
                }
            }
        }
    }
}