package co.bjkarper.comics_detail_data.obj

import co.bjkarper.features.comic_data.obj.Comic
import co.bjkarper.features.series_data.obj.Series

interface IComicDetail {
    val comicId: Int
    val title: String
    var description: String
    var imageUrl: String
}

class ComicDetail(
    override val comicId: Int = 0,
    override val title: String = "",
    override var description: String = "",
    override var imageUrl: String = ""
) : IComicDetail {

    constructor(comic: Comic, series: Series) : this(
        comic.id,
        comic.title,
        comic.description.ifEmpty { series.description },
        comic.images.firstNotNullOfOrNull { it.path } ?: ""
    )
}