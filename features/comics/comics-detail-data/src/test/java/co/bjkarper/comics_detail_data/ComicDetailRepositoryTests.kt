package co.bjkarper.comics_detail_data

import co.bjkarper.comics_detail_data.repo.ComicDetailRepository
import co.bjkarper.features.comic_data.obj.Comic
import co.bjkarper.features.comic_data.obj.ComicDataContainer
import co.bjkarper.features.comic_data.obj.ComicDataWrapper
import co.bjkarper.features.comic_data.repo.IComicRepository
import co.bjkarper.features.series_data.ISeriesRepository
import co.bjkarper.features.series_data.obj.Series
import co.bjkarper.features.series_data.obj.SeriesDataContainer
import co.bjkarper.features.series_data.obj.SeriesDataWrapper
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.HttpException
import retrofit2.Response


@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class ComicDetailRepositoryTests {

    private val mockComicRepository = mockk<IComicRepository>()
    private val mockSeriesRepository = mockk<ISeriesRepository>()

    private val comicDetailRepository =
        ComicDetailRepository(mockComicRepository, mockSeriesRepository)

    private var testComic =
        Comic(id = 1, title = "Test Comic Title", description = "Test Comic Description")
    private var testComicDataContainer = ComicDataContainer(count = 1, results = listOf(testComic))
    private val testComicDataWrapper =
        ComicDataWrapper(code = "Ok", status = "200", data = testComicDataContainer)

    private val testSeries =
        Series(id = 1, title = "Test Series Title", description = "Test Series Description")
    private val testSeriesDataContainer =
        SeriesDataContainer(count = 1, results = listOf(testSeries))
    private val testSeriesDataWrapper =
        SeriesDataWrapper(code = "Ok", status = "200", data = testSeriesDataContainer)

    @Test
    fun `Test getComicDetailById Returns Valid with Comic Description`() = runTest {
        coEvery { mockComicRepository.getComicById(1) } returns flow {
            Result.success(
                testComicDataWrapper
            )
        }
        coEvery { mockSeriesRepository.getSeriesById(1) } returns flow {
            Result.success(
                testSeriesDataWrapper
            )
        }

        comicDetailRepository.getComicDetailById(1).collect { result ->
            result.getOrNull()?.let {
                Assert.assertTrue(it.title == "Test Comic Title")
                Assert.assertTrue(it.description == "Test Comic Description")
            }
        }
    }

    @Test
    fun `Test getComicDetailById Returns Valid with Series Description`() = runTest {
        testComic = Comic(id = 1, title = "Test Comic Title")

        coEvery { mockComicRepository.getComicById(1) } returns flow {
            Result.success(
                testComicDataWrapper
            )
        }
        coEvery { mockSeriesRepository.getSeriesById(1) } returns flow {
            Result.success(
                testSeriesDataWrapper
            )
        }

        comicDetailRepository.getComicDetailById(1).collect { result ->
            result.getOrNull()?.let {
                Assert.assertTrue(it.title == "Test Comic Title")
                Assert.assertTrue(it.description == "Test Series Description")
            }
        }
    }

    @Test
    fun `Test getComicById Returns Error`() = runTest {
        coEvery { mockComicRepository.getComicById(1) } throws HttpException(
            Response.error<Any>(
                409,
                "Conflict".toResponseBody()
            )
        )

        comicDetailRepository.getComicDetailById(1).catch {
            Assert.assertTrue(it::class == HttpException::class)
            Assert.assertTrue(it.message == "Conflict")
        }
    }
}