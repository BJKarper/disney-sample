plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("dagger.hilt.android.plugin")
    kotlin("kapt")
}

android {
    namespace = "co.bjkarper.sample.features.comics_detail"
    compileSdk = AndroidVersions.COMPILE_SDK

    defaultConfig {
        minSdk = AndroidVersions.MINIMUM_SDK
        targetSdk = AndroidVersions.TARGET_SDK

        testInstrumentationRunner = "co.bjkarper.features.comics_detail.HiltTestRunner"
        consumerProguardFiles("consumer-rules.pro")
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    kapt {
        correctErrorTypes = true
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Compose.VERSION
    }
}

dependencies {

    implementation(project(":comics-detail-data"))

    implementation(Kotlin.COROUTINES)
    implementation(kotlin("reflect"))

    implementation(Compose.UI)
    implementation(Compose.MATERIAL)
    implementation(Compose.FOUNDATION)
    implementation(Compose.MATERIAL)
    implementation(Compose.LIFECYCLE_VIEWMODEL)
    implementation(Compose.TOOLING)

    implementation(ComposeNavigation.NAVIGATION)

    implementation(Coil.COIL)
    implementation(Coil.COIL_COMPOSE)

    implementation(Dagger.HILT)
    kapt(Dagger.HILT_COMPILER)

    testImplementation(JUnit.JUNIT)
    testImplementation(Kotlin.COROUTINES)
    testImplementation(JUnit.COROUTINES_JUNIT)
    testImplementation(MockK.MOCKK)

    androidTestImplementation(JUnit.COMPOSE_JUNIT)
    androidTestImplementation(JUnit.COMPOSE_TEST)
    androidTestImplementation(AndroidTest.ANDROID_RUNNER)
    androidTestImplementation(AndroidTest.HILT_ANDROID_TEST)
    kaptAndroidTest(Dagger.HILT_COMPILER)

    androidTestImplementation(MockK.MOCKK_ANDROID)
}