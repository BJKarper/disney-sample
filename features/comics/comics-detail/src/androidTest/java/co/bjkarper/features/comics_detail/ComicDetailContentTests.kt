package co.bjkarper.features.comics_detail

import androidx.compose.foundation.layout.Column
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import co.bjkarper.comics_detail_data.obj.ComicDetail
import co.bjkarper.comics_detail_data.obj.IComicDetail
import co.bjkarper.features.comics_detail.ui.ComicDetailContent
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@OptIn(ExperimentalCoroutinesApi::class)
class ComicDetailContentTests {

    @get:Rule(order = 1)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    val composeTestRule = createComposeRule()

    @BindValue
    @JvmField
    val viewModel = mockk<ComicDetailViewModel>(relaxed = true)

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun testComicDetailDisplay() {
        val comic = ComicDetail(
            title = "Test Title",
            description = "Test Description",
            imageUrl = "Test Url"
        )
        val comicFlow = MutableSharedFlow<IComicDetail>()
        coEvery { viewModel.currentComic } returns comicFlow

        composeTestRule.setContent {
            Column {
                ComicDetailContent(comicDetailViewModel = viewModel)
            }
        }

        runTest {
            comicFlow.emit(comic)
        }

        composeTestRule.onNodeWithTag("ComicDetailImage").assertExists()
        composeTestRule.onNodeWithTag("ComicDetailTitle").assertExists()
        composeTestRule.onNodeWithTag("ComicDetailDescription").assertExists()
    }

    @Test
    fun testComicDetailDisplayMissingTitle() {
        val comic = ComicDetail(
            title = "",
            description = "Test Description",
            imageUrl = "Test Url"
        )
        val comicFlow = MutableSharedFlow<IComicDetail>()
        coEvery { viewModel.currentComic } returns comicFlow

        composeTestRule.setContent {
            ComicDetailContent(comicDetailViewModel = viewModel)
        }

        runTest {
            comicFlow.emit(comic)
        }

        composeTestRule.onNodeWithTag("ComicDetailImage").assertExists()
        composeTestRule.onNodeWithTag("ComicDetailTitle").assertDoesNotExist()
        composeTestRule.onNodeWithTag("ComicDetailDescription").assertExists()
    }

    @Test
    fun testComicDetailDisplayMissingDescription() {
        val comic = ComicDetail(
            title = "Test Title",
            description = "",
            imageUrl = "Test Url"
        )
        val comicFlow = MutableSharedFlow<IComicDetail>()
        coEvery { viewModel.currentComic } returns comicFlow

        composeTestRule.setContent {
            ComicDetailContent(comicDetailViewModel = viewModel)
        }

        runTest {
            comicFlow.emit(comic)
        }

        composeTestRule.onNodeWithTag("ComicDetailImage").assertExists()
        composeTestRule.onNodeWithTag("ComicDetailTitle").assertExists()
        composeTestRule.onNodeWithTag("ComicDetailDescription").assertDoesNotExist()
    }

    @Test
    fun testComicDetailDisplayMissingImage() {
        val comic = ComicDetail(
            title = "Test Title",
            description = "Test Description",
            imageUrl = ""
        )
        val comicFlow = MutableSharedFlow<IComicDetail>()
        coEvery { viewModel.currentComic } returns comicFlow

        composeTestRule.setContent {
            ComicDetailContent(comicDetailViewModel = viewModel)
        }

        runTest {
            comicFlow.emit(comic)
        }

        composeTestRule.onNodeWithTag("ComicDetailImage").assertDoesNotExist()
        composeTestRule.onNodeWithTag("ComicDetailTitle").assertExists()
        composeTestRule.onNodeWithTag("ComicDetailDescription").assertExists()
    }
}