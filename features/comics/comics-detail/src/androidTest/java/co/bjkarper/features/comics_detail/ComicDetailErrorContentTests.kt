package co.bjkarper.features.comics_detail

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import co.bjkarper.features.comics_detail.ui.ComicDetailErrorContent
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@OptIn(ExperimentalCoroutinesApi::class)
class ComicDetailErrorContentTests {

    @get:Rule(order = 1)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    val composeTestRule = createComposeRule()

    @BindValue
    @JvmField
    val viewModel = mockk<ComicDetailViewModel>(relaxed = true)

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun testComicDetailError() {
        val comicFlow = MutableStateFlow(false)
        coEvery { viewModel.showError } returns comicFlow

        composeTestRule.setContent {
            val showError by viewModel.showError.collectAsState(initial = false)
            if (showError) {
                ComicDetailErrorContent()
            }
        }

        runTest {
            comicFlow.emit(true)
        }

        composeTestRule.onNodeWithTag("ComicDetailError").assertExists()
    }
}