package co.bjkarper.features.comics_detail

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.bjkarper.comics_detail_data.obj.IComicDetail
import co.bjkarper.comics_detail_data.repo.IComicDetailRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ComicDetailViewModel @Inject constructor(
    private val comicRepository: IComicDetailRepository
) : ViewModel() {

    private val _currentComic = MutableSharedFlow<IComicDetail>()
    val currentComic = _currentComic.asSharedFlow()

    private val _showError = MutableStateFlow(false)
    val showError = _showError.asStateFlow()

    private val _isLoading = MutableStateFlow(true)
    val isLoading = _isLoading.asStateFlow()

    private var _comicId = 0

    fun getComicById(comicId: Int) {
        _comicId = comicId
        comicRepository.getComicDetailById(comicId).onEach { result ->
            if (result.isSuccess) {
                result.getOrNull()?.let { details ->
                    _currentComic.emit(details)
                }
            } else {
                toggleError(true)
            }
        }.onStart {
            toggleError(false)
            showLoading(true)
        }.onCompletion {
            showLoading(false)
        }.catch {
            toggleError(true)
        }.launchIn(viewModelScope)
    }

    fun refresh() {
        Log.d("BJKARPER", "REFRESHING")
        if (_comicId != 0) {
            getComicById(_comicId)
        }
    }

    private fun toggleError(show: Boolean) {
        viewModelScope.launch {
            _showError.emit(show)
        }
    }

    private fun showLoading(show: Boolean) {
        viewModelScope.launch {
            _isLoading.emit(show)
        }
    }
}