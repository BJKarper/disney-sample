package co.bjkarper.features.comics_detail.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ComicDetailErrorContent() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        Text(
            text = "There was an error loading. Please try again.",
            fontSize = 16.sp,
            textAlign = TextAlign.Center,
            color = Color.Red,
            modifier = Modifier
                .fillMaxWidth(8f)
                .wrapContentHeight()
                .padding(horizontal = 12.dp, vertical = 20.dp)
                .testTag("ComicDetailError")
        )
    }
}