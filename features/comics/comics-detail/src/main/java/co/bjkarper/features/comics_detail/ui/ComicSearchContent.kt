package co.bjkarper.features.comics_detail.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import co.bjkarper.features.comics_detail.ComicDetailViewModel

@Composable
fun ComicSearchContent(
    comicDetailViewModel: ComicDetailViewModel,
    navController: NavController = rememberNavController()
) {
    var searchText by remember { mutableStateOf("") }
    var isEnabled by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(12.dp)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .wrapContentSize()
                .align(Alignment.Center)
        ) {
            Text(
                text = "Enter a Comic book Id",
                fontSize = 16.sp,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .fillMaxWidth(.8f)
                    .wrapContentHeight()
                    .padding(bottom = 12.dp)
                    .testTag("ComicSearchText")
            )
            TextField(
                value = searchText,
                onValueChange = {
                    searchText = it
                    isEnabled = it.isNotEmpty()
                },
                modifier = Modifier
                    .fillMaxWidth(.8f)
                    .padding(bottom = 12.dp)
                    .testTag("ComicSearchTextField")
            )
            Button(
                enabled = isEnabled,
                onClick = {
                    comicDetailViewModel.getComicById(searchText.toInt())
                    navController.navigate("Details")
                },
                modifier = Modifier
                    .fillMaxWidth(.5f)
                    .height(48.dp)
                    .testTag("ComicSearchButton")
            ) {
                Text(text = "Search")
            }
        }
    }
}