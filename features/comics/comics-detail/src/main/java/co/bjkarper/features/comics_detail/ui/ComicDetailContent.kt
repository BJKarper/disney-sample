package co.bjkarper.features.comics_detail.ui

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import co.bjkarper.features.comics_detail.ComicDetailViewModel
import coil.compose.AsyncImage
import coil.request.ImageRequest

@Composable
fun ComicDetailContent(
    comicDetailViewModel: ComicDetailViewModel,
    navController: NavController = rememberNavController()
) {
    val currentComic by comicDetailViewModel.currentComic.collectAsState(initial = null)

    BackHandler {
        navController.popBackStack()
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        currentComic?.let { comic ->
            if (comic.imageUrl.isNotEmpty()) {
                ComicImageSection(
                    imageUrl = comic.imageUrl,
                    contentDescription = comic.title
                )
            }

            if (comic.title.isNotEmpty()) {
                ComicTitleSection(title = comic.title)
            }

            if (comic.description.isNotEmpty()) {
                ComicDescriptionSection(description = comic.description)
            }
        }
    }
}

@Composable
fun ComicImageSection(imageUrl: String, contentDescription: String) {
    AsyncImage(
        model = ImageRequest.Builder(LocalContext.current)
            .data("${imageUrl}/portrait_uncanny.jpg")
            .build(),
        contentDescription = "$contentDescription cover image",
        contentScale = ContentScale.FillWidth,
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .testTag("ComicDetailImage")
    )
}

@Composable
fun ComicTitleSection(title: String) {
    Text(
        text = title,
        fontSize = 20.sp,
        fontWeight = FontWeight.Bold,
        textAlign = TextAlign.Start,
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(horizontal = 12.dp, vertical = 8.dp)
            .testTag("ComicDetailTitle")
    )
}

@Composable
fun ComicDescriptionSection(description: String) {
    Text(
        text = description,
        fontSize = 16.sp,
        textAlign = TextAlign.Start,
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(horizontal = 12.dp, vertical = 8.dp)
            .testTag("ComicDetailDescription")
    )
}