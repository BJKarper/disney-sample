package co.bjkarper.features.comics_detail

import co.bjkarper.comics_detail_data.obj.ComicDetail
import co.bjkarper.comics_detail_data.repo.IComicDetailRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class ComicDetailViewModelTests {

    @get:Rule
    @OptIn(ExperimentalCoroutinesApi::class)
    val mainCoroutineRule = MainCoroutineRule()

    private val mockComicRepository = mockk<IComicDetailRepository>()
    private val testComicDetail = ComicDetail(title = "Test Comic Title")

    private val viewModel = ComicDetailViewModel(mockComicRepository)

    @Before
    fun setup() {
        coEvery { mockComicRepository.getComicDetailById(1) } returns flow {
            emit(Result.success(testComicDetail))
        }
    }

    @Test
    fun `Test getComicById Returns Valid`() = runTest {
        viewModel.getComicById(1)

        mainCoroutineRule.launch {
            viewModel.currentComic.collect {
                Assert.assertTrue(it.title == testComicDetail.title)
            }
        }
    }

    @Test
    fun `Test isLoading toggles Correctly`() = runTest {
        viewModel.getComicById(1)

        mainCoroutineRule.launch {
            Assert.assertTrue(viewModel.isLoading.count() == 2)
        }
    }

    @Test
    fun `Test showError toggles onError`() = runTest {
        coEvery { mockComicRepository.getComicDetailById(1) } throws Exception()

        mainCoroutineRule.launch {
            Assert.assertTrue(viewModel.showError.value)
        }
    }
}