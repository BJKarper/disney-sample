package co.bjkarper.features.comic_data

import co.bjkarper.features.comic_data.api.ComicApi
import co.bjkarper.features.comic_data.obj.Comic
import co.bjkarper.features.comic_data.obj.ComicDataContainer
import co.bjkarper.features.comic_data.obj.ComicDataWrapper
import co.bjkarper.features.comic_data.repo.ComicRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.HttpException
import retrofit2.Response


@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class ComicRepositoryTests {

    private val mockApi = mockk<ComicApi>()

    private val comicRepository = ComicRepository(mockApi)

    private val testComic =
        Comic(id = 1, title = "Test Comic Title", description = "Test Comic Description")
    private val testComicDataContainer = ComicDataContainer(count = 1, results = listOf(testComic))
    private val testComicDataWrapper =
        ComicDataWrapper(code = "Ok", status = "200", data = testComicDataContainer)

    @Test
    fun `Test getComicById Returns Valid`() = runTest {
        coEvery { mockApi.getComicById(1) } returns testComicDataWrapper

        comicRepository.getComicById(1).collect { result ->
            result.getOrNull()?.let {
                Assert.assertTrue(it.code == "Ok")
                Assert.assertTrue(it.data.count == 1)
                Assert.assertTrue(it.data.results.first().id == 1)
            }
        }
    }

    @Test
    fun `Test getComicById Returns Error`() = runTest {
        coEvery { mockApi.getComicById(1) } throws HttpException(
            Response.error<Any>(
                409,
                "Conflict".toResponseBody()
            )
        )

        comicRepository.getComicById(1).catch {
            Assert.assertTrue(it::class == HttpException::class)
            Assert.assertTrue(it.message == "Conflict")
        }
    }
}