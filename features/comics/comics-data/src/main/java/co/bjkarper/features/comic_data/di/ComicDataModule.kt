package co.bjkarper.features.comic_data.di


import co.bjkarper.features.comic_data.api.ComicApi
import co.bjkarper.features.comic_data.repo.ComicRepository
import co.bjkarper.features.comic_data.repo.IComicRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import retrofit2.Retrofit

@Module
@InstallIn(ViewModelComponent::class)
class ComicDataModule {

    @Provides
    @ViewModelScoped
    fun providesComicRepository(api: ComicApi): IComicRepository {
        return ComicRepository(api)
    }

    @Provides
    @ViewModelScoped
    fun providesComicAPI(retrofit: Retrofit): ComicApi {
        return retrofit.create(ComicApi::class.java)
    }

}