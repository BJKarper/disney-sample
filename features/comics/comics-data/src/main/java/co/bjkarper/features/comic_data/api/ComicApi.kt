package co.bjkarper.features.comic_data.api

import co.bjkarper.core.network_core.BASE_COMICS_URL
import co.bjkarper.features.comic_data.obj.ComicDataWrapper
import retrofit2.http.GET
import retrofit2.http.Path

interface ComicApi {

    @GET("$BASE_COMICS_URL/{comicId}")
    suspend fun getComicById(@Path("comicId") comicId: Int): ComicDataWrapper
}