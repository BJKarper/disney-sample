package co.bjkarper.features.comic_data.obj

import co.bjkarper.core.network_core.obj.IResource
import co.bjkarper.core.network_core.obj.Image
import co.bjkarper.core.network_core.obj.Resource
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Comic(

    @JsonProperty("id") val id: Int = 0,
    @JsonProperty("title") val title: String = "",
    @JsonProperty("description") val description: String = "",
    @JsonProperty("thumbnail") val thumbnail: Image = Image(),
    @JsonProperty("images") val images: List<Image> = emptyList(),
    @JsonProperty("series") val series: IResource = Resource()
)
