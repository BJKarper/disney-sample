package co.bjkarper.features.comic_data.repo

import co.bjkarper.features.comic_data.api.ComicApi
import co.bjkarper.features.comic_data.obj.ComicDataWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

interface IComicRepository {

    fun getComicById(comicId: Int): Flow<Result<ComicDataWrapper>>
}

class ComicRepository @Inject constructor(
    private val comicApi: ComicApi
) : IComicRepository {

    override fun getComicById(comicId: Int): Flow<Result<ComicDataWrapper>> = flow {
        emit(Result.success(comicApi.getComicById(comicId)))
    }.catch { e ->
        emit(Result.failure(e))
    }.flowOn(Dispatchers.IO)

}